#/bin/sh

mkdir .testtmp &&
# Compiler tests
echo "Installing requirements (for offline testing, redundant in pipeline):" &&
python3 -m pip install -r requirements.txt &&
echo "Compiler tests:" &&
python3 -m compic10 tests/vm_tests/test1.ic10 -o .testtmp/test1_no.asm --silent --no_optimization &&
python3 -m compic10 tests/vm_tests/test1.ic10 -o .testtmp/test1.asm --silent &&
python3 -m compic10 tests/vm_tests/inout.ic10 -o .testtmp/inout_no.asm --silent --no_optimization &&
python3 -m compic10 tests/vm_tests/inout.ic10 -o .testtmp/inout.asm --silent &&
python3 -m compic10.vm .testtmp/test1_no.asm -e tests/vm_tests/test1.yml &&
python3 -m compic10.vm .testtmp/test1.asm -e tests/vm_tests/test1.yml &&
python3 -m compic10.vm .testtmp/inout.asm -e tests/vm_tests/inout.yml &&
python3 -m compic10.vm .testtmp/inout_no.asm -e tests/vm_tests/inout.yml &&

echo &&
echo "PPO tests:" &&
# PPO tests
python3 -m unittest tests/test_ppo.py &&

echo &&
echo "Cleanup" &&
rm -r .testtmp
