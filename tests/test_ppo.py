import os.path
import unittest

from compic10.codegen import create_asm_code
from compic10.postprocess import PostProcessor
from compic10.programdatabase import ProgramDatabase
from compic10.syntax import syntactic_analysis
from compic10.tokens import resolve_tokens


class PostProcessorTestCase(unittest.TestCase):
	def setUp(self):
		# using default startup environment
		#TODO: make default environment static class\var and use in __main__ and there
		ProgramDatabase.VAR_STACK_LENGTH = 400
		ProgramDatabase.VAR_STACK_OFFSET = 100
		ProgramDatabase.ANNOTATE = False
		ProgramDatabase.CODE_OPTIMIZATION = True
		ProgramDatabase.PP_OPTIMIZATION = True

	def load_data(self, source_fn, asm_fn):
		with open(os.path.join(os.path.dirname(__file__), "data", source_fn)) as fp:
			s = fp.read().split("\n")
		with open(os.path.join(os.path.dirname(__file__), "data", asm_fn)) as fp:
			a = fp.read()
		return s, a

	def compile_ppo(self, program):
		code = []
		for i, line in enumerate(program):
			ProgramDatabase.set_compiler_position(i)
			tokens = resolve_tokens(line)
			syntax = syntactic_analysis(tokens)
			code.append(syntax)
		pd = ProgramDatabase(len(code))
		asm_code = create_asm_code(code, ProgramDatabase.BType.Global_, database = pd)[:-1]  # ommit final newline

		#TODO: move base optimization code to func and use in __main__ and there
		asm_code = asm_code.replace("move ra sp\nmove sp ra\n", "")  # can't always handled within a single expression
		asm_code = asm_code.replace("move sp ra\nmove ra sp\n", "")  # can't always handled within a single expression
		asm_code = asm_code.replace("j main\nmain:\n", "")

		pp = PostProcessor(pd, asm_code)
		for i in range(11):
			if not pp.iterate():
				break
		return pp.get_final_code()

	def test_example1(self):
		src, outp = self.load_data("example1.ic10lang", "example1.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_example2(self):
		src, outp = self.load_data("example2.ic10lang", "example2.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_example3(self):
		src, outp = self.load_data("example3.ic10lang", "example3.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_vars__var_not_used1(self):
		src, outp = self.load_data("vars__var_not_used1.ic10lang", "vars__var_not_used1.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_vars__var_not_used2(self):
		src, outp = self.load_data("vars__var_not_used2.ic10lang", "vars__var_not_used2.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_vars__var_used1(self):
		src, outp = self.load_data("vars__var_used1.ic10lang", "vars__var_used1.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_vars__all_var_used1(self):
		src, outp = self.load_data("vars__all_var_used1.ic10lang", "vars__all_var_used1.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_vars__var_used2(self):
		src, outp = self.load_data("vars__var_used2.ic10lang", "vars__var_used2.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_vars__var_used3(self):
		src, outp = self.load_data("vars__var_used3.ic10lang", "vars__var_used3.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_bigscript1(self):
		src, outp = self.load_data("bigscript1.ic10lang", "bigscript1.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))

	def test_bigscript2(self):
		src, outp = self.load_data("control-warning-systems.ic10lang", "control-warning-systems.ic10asm")
		self.assertEqual(outp, self.compile_ppo(src))


if __name__ == '__main__':
	unittest.main()
