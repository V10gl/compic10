@echo off
set PYTHONPATH=.
mkdir .testtmp

rem Compiler tests
echo Installing requirements (for offline testing, redundant in pipeline):
python -m pip install -r requirements.txt
echo Compiler tests:
python -m compic10 tests/vm_tests/test1.ic10 -o .testtmp/test1_no.asm --silent --no_optimization
python -m compic10 tests/vm_tests/test1.ic10 -o .testtmp/test1.asm --silent
python -m compic10 tests/vm_tests/inout.ic10 -o .testtmp/inout_no.asm --silent --no_optimization
python -m compic10 tests/vm_tests/inout.ic10 -o .testtmp/inout.asm --silent
python -m compic10.vm .testtmp/test1_no.asm -e tests/vm_tests/test1.yml
python -m compic10.vm .testtmp/test1.asm -e tests/vm_tests/test1.yml
python -m compic10.vm .testtmp/inout.asm -e tests/vm_tests/inout.yml
python -m compic10.vm .testtmp/inout_no.asm -e tests/vm_tests/inout.yml

echo PPO tests:
rem PPO tests
python -m unittest tests/test_ppo.py

echo Cleanup
rmdir /S /Q .testtmp
